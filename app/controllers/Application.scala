package controllers

import org.jboss.netty.handler.codec.rtsp.RtspHeaders.Values
import play.api._
import play.api.cache.Cached
import play.api.libs.iteratee.Enumerator
import play.api.mvc._
import play.api.data.Form
import play.api.data.Forms._
import Play.current
import models._
import java.io._

object Application extends Controller with Security {

  lazy val hddPath = "/mnt/usb"

  def index = Authenticated {
    val hdd = new File(hddPath)
    val videos = if (hdd.exists() && hdd.isDirectory) hdd.listFiles() else Array[File]()
    Ok(views.html.app.index(videos))
  }

  def view(path: String) = Authenticated {
    val video = new File(s"$hddPath/$path")
    if (video.exists && video.isFile) {
      Ok(views.html.app.view(path))
    } else NotFound
  }

  def video(path: String) = Authenticated { implicit request =>
    val file = new File(s"$hddPath/$path")
    if (file.exists && file.isFile) {
      import scala.concurrent.ExecutionContext.Implicits.global
      val fileContent: Enumerator[Array[Byte]] = Enumerator.fromFile(file)
      Result(
        header = ResponseHeader(OK, Map(
          CONTENT_LENGTH -> file.length.toString,
          CONTENT_RANGE -> s"bytes */${file.length.toString}",
          ACCEPT_RANGES -> Values.BYTES,
          CONTENT_TYPE -> "video/mp4",
          PRAGMA -> Values.PUBLIC,
          CONTENT_TRANSFER_ENCODING -> "binary",
          CONTENT_DISPOSITION -> "attachment"
        )),
        body = fileContent
      )
    } else NotFound
  }

  def login = Cached("login") {
    Action {  implicit request => Ok(views.html.auth.index(loginForm)) }
  }

  def authenticate = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      errors => BadRequest(views.html.auth.index(errors)),
      login => Redirect(routes.Application.index()).withSession("username" -> "admin")
    )
  }

  def logout = Authenticated {
    Redirect(routes.Application.login()).withNewSession
  }

  lazy val loginForm = Form(
    mapping(
      "username" -> nonEmptyText,
      "password" -> nonEmptyText
    )(Login.apply)(Login.unapply)
    verifying("Os seus dados de autenticação estão incorretos.", login => login.username == "admin" && login.password == "demo")
  )
}