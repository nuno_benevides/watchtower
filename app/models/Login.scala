package models

import play.api.mvc.Security.AuthenticatedBuilder

case class Login(username: String, password: String)

trait Security {
  object Authenticated extends AuthenticatedBuilder(req => req.session.get("username"))
}